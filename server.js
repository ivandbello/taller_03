var express = require('express');
var Menor = require('./menor.js')
var app = express();

app.use(function(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

app.get('/menor/:number1/:number2', function(req, res){
	var number1 = req.params.number1
	var number2 = req.params.number2
	var numberr = 0
	let menor = new Menor(number1,number2);

	let result_body = {numberr: numberr, function: 'menor', result: menor.menor}

	res.json(result_body)
})


app.set('port', (process.env.PORT || 8080));


app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});